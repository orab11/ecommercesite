import Componentlist from './Component/Componentlist';
import logo from './logo.svg';


function App() {
  return (
    <>
      <section>
        <Componentlist></Componentlist>
      </section>
    </>
  );
}

export default App;
